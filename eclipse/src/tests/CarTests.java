package tests;
import automobiles.Car;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CarTests {

	@Test
	public void getTests() {
		Car testCar = new Car(10);
		assertEquals(10, testCar.getSpeed());
		assertEquals(50, testCar.getLocation());
	}
	
	@Test
	public void moveRightTest() {
		Car testCar = new Car(30);
		testCar.moveRight();
		assertEquals(80, testCar.getLocation());
	}
	
	@Test
	public void moveRightIfTest() {
		Car testCar = new Car(100);
		testCar.moveRight();
		assertEquals(100, testCar.getLocation());
	}
	
	@Test
	public void moveLeftTest() {
		Car testCar = new Car(40);
		testCar.moveLeft();
		assertEquals(10, testCar.getLocation());
	}

	@Test
	public void moveLeftIfTest() {
		Car testCar = new Car(100);
		testCar.moveLeft();
		assertEquals(0, testCar.getLocation());
	}
	
	@Test
	public void accelerateTest() {
		Car testCar = new Car(20);
		testCar.accelerate();
		assertEquals(21, testCar.getSpeed());
	}
	
	@Test
	public void stopTest() {
		Car testCar = new Car(20);
		testCar.stop();
		assertEquals(0, testCar.getSpeed());
	}
	
	@Test
	public void negativeTest() {
		Car testCar = new Car(-20);
		fail ("negative speed");
	}
}
